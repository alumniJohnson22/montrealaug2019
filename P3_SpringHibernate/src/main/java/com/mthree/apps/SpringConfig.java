package com.mthree.apps;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class SpringConfig {
	
	
	 // Configuring SessionFactory bean using LocalSessionFactoryBean class
	  @Bean
	    public LocalSessionFactoryBean sessionFactory() {
	        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	        //this ugliness is why reading a properties file is better
	        //sessionFactory.setDataSource(dataSource());
	        sessionFactory.setDataSource(outsideDataSource());
	        sessionFactory.setPackagesToScan(new String[] {"com.mthree.apps"}); // Scans the base packages
	        sessionFactory.setHibernateProperties(hibernateProperties());
	        return sessionFactory;
	    }
	 
	  // hard-coded, ick. SpringConfig so ok. But, ick. Better: read from properites somewhere
	  // Configuring a new datasource bean definition using Apache's BasicDataSource (well known for connection pooling)
	    @Bean
	    public DataSource dataSource() {
	        BasicDataSource dataSource = new BasicDataSource();
	        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	        dataSource.setUrl("jdbc:oracle:thin:@10.20.40.53:1521:oradb1");
	        dataSource.setUsername("delegate");
	        dataSource.setPassword("pass");
	        dataSource.setMaxActive(5); // Sets the maximum number of active connections that can be allocated at the same time.
	        return dataSource;
	    }
	 
	    @Bean
	    public DataSource outsideDataSource() {
	        BasicDataSource dataSource = new BasicDataSource();
	        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	        dataSource.setUrl("jdbc:oracle:thin:@88.211.122.42:1521:oradb1");
	        dataSource.setUsername("delegate");
	        dataSource.setPassword("pass");
	        dataSource.setMaxActive(5); // Sets the maximum number of active connections that can be allocated at the same time.
	        return dataSource;
	    }
	    
	    
	    @Bean
	    public PlatformTransactionManager hibernateTransactionManager() {
	        HibernateTransactionManager transactionManager
	          = new HibernateTransactionManager();
	        transactionManager.setSessionFactory(sessionFactory().getObject());
	        return transactionManager;
	    }
	 
	    
	    // Configuring the Hibernate properties
	    private final Properties hibernateProperties() {
	        Properties hibernateProperties = new Properties();
	        hibernateProperties.setProperty(
	          "hibernate.hbm2ddl.auto", "create"); // default is "update", create recreates tables every time we execute the application
	        /*
	        
			    create :- Hibernate creates the schema, the data previously present (if there) in the schema is lost
				update:- Hibernate updates the schema with the given values.
				validate:- Hibernate validates the schema. It makes no change in the DB.
				create-drop:- create the schema with destroying the data previously present(if there). It also drop the database schema when the SessionFactory is closed.
            */
	        
	        hibernateProperties.setProperty(
	          "hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
	        
	        hibernateProperties.setProperty(
	  	          "hibernate.show_sql", "true"); // To log HQL within the console
	 
	        return hibernateProperties;
	    }

}

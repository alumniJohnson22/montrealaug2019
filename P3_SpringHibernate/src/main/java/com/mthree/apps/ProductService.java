package com.mthree.apps;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//either retrieve this service from the context by class name or 
//retrieve it by the name given in the Service annotation
//or neither if autowired
@Service("productService")
public class ProductService {

	@Autowired
	private SessionFactory sessionFactory;

	//use a transaction to add some elements to a table
	public boolean storeSomeProductsAndContracts() {

		//                       ProductCode, Name,                  Exchange, Category,       Subcategory   Class,    SZE
		Product p1 = new Product("ED",        "Eurodollar Futures",  "CME",    "InterestRate", "Stirs",      "Future", "NULL");
		Product p2 = new Product("LN",        "Natural Gas Options", "NYMEX",  "Energy",       "NaturalGas", "Option", "MMBtu");
		Product p3 = new Product("MO",        "Minerals Options",    "NYMEX",  "Commodities",  "Zinc",       "Option", "letters");
		
		LocalDate contractStartDate1 = LocalDate.of(2006, Month.SEPTEMBER, 18);
		LocalDate contractEndDate1 = LocalDate.of(2016, Month.SEPTEMBER, 19);

		LocalDate contractStartDate2 = LocalDate.of(2008, Month.DECEMBER, 23);
		LocalDate contractEndDate2 = LocalDate.of(2016, Month.AUGUST, 26);

		Contract c1 = new Contract();
		c1.setSym("EDU16");
		c1.setStartDate(contractStartDate1);
		c1.setEndDate(contractEndDate1);

		Contract c2 = new Contract();
		c2.setSym("LNU16");
		c2.setStartDate(contractStartDate2);
		c2.setEndDate(contractEndDate2);

		Contract c3 = new Contract();
		c3.setSym("MOX16");
		c3.setStartDate(contractStartDate1);
		c3.setEndDate(contractEndDate1);
		
		Contract c4 = new Contract();
		c4.setSym("MOU16");
		c4.setStartDate(contractStartDate2);
		c4.setEndDate(contractEndDate2);
		
		Set<Contract> contractsForProduct1 = new HashSet<Contract>();
		contractsForProduct1.add(c1);

		Set<Contract> contractsForProduct2 = new HashSet<Contract>();
		contractsForProduct2.add(c2);
		
		Set<Contract> contractsForProduct3 = new HashSet<Contract>();
		contractsForProduct3.add(c3);
		contractsForProduct3.add(c4);

		p1.setContracts(contractsForProduct1);

		p2.setContracts(contractsForProduct2);
		
		p3.setContracts(contractsForProduct3);

		Session session = sessionFactory.openSession();

		// Begin transaction

		Transaction tx = session.beginTransaction();

		// the database will assign an identifier and the session will return it
		String productCode1 = (String) session.save(p1);
		String productCode2 = (String) session.save(p2);
		String productCode3 = (String) session.save(p3);
		
		c1.setProductCode(p1);
		c2.setProductCode(p2);
		c3.setProductCode(p3);
		c4.setProductCode(p3);
		
		session.save(c1);
		session.save(c2);
		session.save(c3);
		session.save(c4);

		tx.commit();

		// don't write to the screen: ick.
		System.out.println("Session: " + session.hashCode() + " Open Status after commit: " + session.isOpen() + " Connection Status after commit: " + session.isConnected()); 

		if (productCode1 != null || productCode2 != null || productCode3 != null) {
			return true;

		} else {
			return false;
		}

	}

	public List<Product> getProductList() {

		// Create an Session using Session Factory

		Session session = sessionFactory.openSession();

		// Create a typed query, a cast is unnecessary
		// Spring will use the Product.class as the type of the list returned
		TypedQuery<Product> query = session.createQuery("from Product", Product.class);

		List<Product> products = query.getResultList();
		
		// don't write to the screen: ick.
		System.out.println("Session: " + session.hashCode() + " Open Status after query: " + session.isOpen() + " Connection Status after query: " + session.isConnected()); 
		
		return products;
	}

	public List<Product> findProductsByName(String name) {

		// Create an Session using Session Factory

		Session session = sessionFactory.openSession();

		// Create a typed query using placeholder and an alias
		// the placeholder is positional
		TypedQuery<Product> query = session.createQuery("from Product p where p.name = ?1", Product.class);
		
		// bind the value by refering to the position following the ?
		query.setParameter(1, name);
		
		List<Product> products = query.getResultList();

		// don't write to the screen: ick.
		System.out.println("Session: " + session.hashCode() + " Open Status after query: " + session.isOpen() + " Connection Status after query: " + session.isConnected()); 

		return products;

	}

	public List<Product> findProductByNameUsingWildCards(String name) {

		// Create an Session using Session Factory

		Session session = sessionFactory.openSession();

		// Create a typed query using placeholder, this placeholder is a bind parameter. 
		TypedQuery<Product> query = session.createQuery("from Product p where p.name like :pName", Product.class);
		
		//bind the parameter by refering to the name following the :
		query.setParameter("pName", name + "%");

		List<Product> products = query.getResultList();

		// don't write to the screen: ick.
		System.out.println("Session: " + session.hashCode() + " Open Status after query: " + session.isOpen() + " Connection Status after query: " + session.isConnected()); 

		return products;

	}

	public Product findProductByCodeUsingNamedNativeQuery(String productCode) {

		// Create an Session using Session Factory

		Session session = sessionFactory.openSession();

		// Create a named query. Refer to entity annotations for the specific query.
		TypedQuery<Product> query = session.createNamedQuery("searchProductByCode", Product.class);

		query.setParameter(1, productCode);
		
		//problem here when code does not exist?
		if (query.getResultList().size() > 0) {
			return query.getResultList().get(0);
		} else {
			return null;
		}

	}

}

package com.mthree.apps;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONTRACT_DUMMY_ELENA", schema = "DELEGATE")
public class Contract {
	
	@Id  // Creates a PRIMARY KEY constraints, all entity classes must have a primary key
	@Column(name="SYM")
	private String sym;
	
	@ManyToOne 
	@JoinColumn(name="productCode")
    private Product productCode;
	
	@Column(name="STARTDATE")
	private LocalDate startDate;
	
	
	@Column(name="ENDDATE")
	private LocalDate endDate;


	public String getSym() {
		return sym;
	}


	public void setSym(String sym) {
		this.sym = sym;
	}


	
	public LocalDate getStartDate() {
		return startDate;
	}


	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}


	public LocalDate getEndDate() {
		return endDate;
	}


	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Product getProductCode() {
		return productCode;
	}


	public void setProductCode(Product productCode) {
		this.productCode = productCode;
	}


	@Override
	public String toString() {
		return "Contract [sym=" + sym + ", productCode=" + productCode + ", startDate=" + startDate + ", endDate="
				+ endDate + "]";
	}

}

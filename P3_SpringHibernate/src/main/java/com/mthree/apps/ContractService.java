package com.mthree.apps;


import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//either retrieve this service from the context by class name or 
//retrieve it by the name given in the Service annotation
//or neither if autowired
@Service("contractService")
public class ContractService {
	
	
	@Autowired 
	private SessionFactory sessionFactory;
	
   	
    public List<Contract> loadAllContracts(){
        
		// Create a Session using Session Factory
		
		Session session = sessionFactory.openSession();
		
		// Create a typed query  Select * is not supported
		// Select c from Contract c makes c the alias of Contract (the second c)
		// It also retrieves all columns from c (the first c)
		TypedQuery<Contract> query = session.createQuery("SELECT c from Contract c JOIN c.productCode p ON p.productCode = c.productCode", Contract.class);
		
		
		return query.getResultList();
    }

}

package com.mthree.apps;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class SpringAndHibernateApplication implements CommandLineRunner{

	@Autowired 
	 ProductService productService;
	
	@Autowired
	ContractService contractService;
	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringAndHibernateApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		
		// Call methods as required
		
		viewProductsAndContracts();
	
		viewContracts();
		
		nativeQueryDemo();
		
		
		
	}
	
	public void viewProductsAndContracts() {
		
		
		if(productService.storeSomeProductsAndContracts())
		{
			System.out.println("Product details stored!!");
		}
		
		else {
			System.out.println("Unable to store products!!!!");
		}
		
		
		System.out.println("Number of products : "+productService.getProductList().size());
		
		List<Product> productsByName = productService.findProductsByName("Eurodollar Futures");
		
		System.out.println("Search result product by name ");
		
		for(Product p : productsByName) {
			
			System.out.println(p);
			
			System.out.println("List of contracts : ");
			
			for(Contract c : p.getContracts()) {
				
				System.out.println(c);
			}
			
		}
		
	}
	public void viewContracts() {
		
	   System.out.println("******************List of Contracts**********************");
		
		List<Contract> allContracts = contractService.loadAllContracts();
		
		System.out.println("Total number of contracts "+allContracts.size());
		
		for(Contract c : allContracts) {
		
			System.out.println(c);
	
		}
		
	}
	
	public void nativeQueryDemo() {
		
        System.out.println("Search product by code ");
		
		String productCode = "LN";
		Product p = productService.findProductByCodeUsingNamedNativeQuery(productCode);
		
		if(p!=null) {
			System.out.println(p);
		}
		else {
			System.out.println("Product " + productCode + " not found!!");
		}
		
//		productCode = "AA";
//		p = productService.findProductByCodeUsingNamedNativeQuery(productCode);
//	
//		if(p!=null) {
//			System.out.println(p);
//		}
//		else {
//			System.out.println("Product " + productCode + " not found!!");
//		}		
		
	}

}


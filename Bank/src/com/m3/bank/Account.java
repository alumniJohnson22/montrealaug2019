package com.m3.bank;

import java.util.UUID;

public class Account {
	private String accountID;
	
	public Account() {
		this.accountID = UUID.randomUUID().toString();
	}
	
	public Account(String accountID){
		this.accountID = accountID;
	}
	
	public String getAccountID() {
		return accountID;
	}
	
}

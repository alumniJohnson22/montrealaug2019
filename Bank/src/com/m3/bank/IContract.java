package com.m3.bank;

import java.math.BigDecimal;

public interface IContract {
	public int getMinChecks();
	public BigDecimal getMinBalance();
}

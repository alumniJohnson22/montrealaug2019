package com.m3.bank;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class Customer {
	private final String customerID;
	private Map<String, Account> accounts;
	
	Customer() {
		this.customerID = UUID.randomUUID().toString();
		this.accounts = new HashMap<>();
	}
	
	Customer(String customerID, Map<String, Account> accounts){
		this.customerID = customerID;
		this.accounts = accounts;
	}

	public String getCustomerID() {
		return customerID;
	}
	
//	public static Customer create() {
//		return new Customer();
//	}

	public Account add(String uuid, Account account) {
		accounts.put(uuid, account);
		return account;
		
	}

	public Account remove(String uuid) {
		return accounts.remove(uuid);
	}
	
	public Account getAccountByID(String uuid) {
		return accounts.get(uuid);
		//TODO: add exception
	}
	
}

package com.m3.bank;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CommercialCustomerTest {
	
	private CommercialCustomer classUnderTest;
	private Account mockAccount; 
	private String customerId = "1111";
	private String accountID = "2222";
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		Map<String, Account> accounts = new HashMap<>();
		
		mockAccount = mock(Account.class);
		when(mockAccount.getAccountID()).thenReturn(accountID);
		accounts.put(mockAccount.getAccountID(), mockAccount);
		
		classUnderTest = new CommercialCustomer(customerId, accounts);
		
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAdd() {
		String msg = "Account " + this.mockAccount.getAccountID() + "could not be retrieved.";
		Account expected = this.mockAccount;
		Account actual = this.classUnderTest.getAccountByID(accountID);
		assertEquals(expected, actual, msg);
	}

	@Test
	void testRemove() {
		String msg = "Account " + this.mockAccount.getAccountID() + "could not be removed.";
		Account expected = this.mockAccount;
		Account actual = this.classUnderTest.remove(accountID);
		assertEquals(expected, actual, msg);
	}
	
	@Test
	void testRemoveNullCheck() {
		String msg = "Account " + this.classUnderTest.getAccountByID(accountID).getAccountID() + "could not be removed because unexpected null.";
		this.classUnderTest.remove(accountID);
		Account actual = this.classUnderTest.getAccountByID(accountID);
		assertNull(actual, msg);
	}
	
	@Test
	void testNotNull() {
		assertNotNull(classUnderTest);
	}

}

package com.m3.training;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CompositionTest {
	
	//@Mock
	private Dependency mockD;
	private Composition classUnderTest;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		mockD = mock(Dependency.class);
		classUnderTest = new Composition(mockD);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		int actual = classUnderTest.generateComposedResult();
		assertTrue(actual < 50);
	}

}

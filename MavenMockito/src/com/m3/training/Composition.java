package com.m3.training;

public class Composition {
	private Dependency dependency;

	public Composition(Dependency dependency) {
		super();
		this.dependency = dependency;
	}
	
	public int generateComposedResult() {
		return this.dependency.generateResult() -50;
	}
}

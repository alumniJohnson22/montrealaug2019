package com.mthree.apps;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages = { "com.mthree.controllers", "com.mthree.configs", "com.mthree.services" })
public class SpringBoot01Application {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(SpringBoot01Application.class, args);
		
		// Explore the application context Spring has created.
		System.out.println("Display name is " + applicationContext.getDisplayName());
		Map<String, Object> beansMap = applicationContext.getBeansOfType(Object.class);
		
//		System.out.println("\n\n\n\nAll Beans");
//		beansMap.entrySet().stream().forEach(entry -> {
//			System.out.println(entry);
//		});
//		System.out.println("End Beans\n\n\n\n");

		System.out.println("\n\n\n\nMThree Beans");
		String beansString = beansMap.entrySet().stream()
				.filter(entry -> entry.getValue().toString().startsWith("com.mthree"))
				.map(entry -> entry.getKey() + " " + entry.getValue().toString()).collect(Collectors.joining(",\n"));
		System.out.println(beansString + "\nEnd Beans\n\n\n\n");
	}

}

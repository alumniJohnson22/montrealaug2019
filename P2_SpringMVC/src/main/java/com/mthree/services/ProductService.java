package com.mthree.services;

import com.mthree.models.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {



    public List<Product> createProductList(){


        Product p1 = new Product("ED","Eurodollar Futures","CME","InterestRate","Stirs","Future","NULL");
        Product p2 = new Product("LN","Natural Gas Options","NYMEX","Energy","NaturalGas","Option","MMBtu");

        List<Product> products = new ArrayList<>();
        products.add(p1);
        products.add(p2);

        return products;



    }



}

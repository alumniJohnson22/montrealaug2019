package com.mthree.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/orders")

public class OrdersController {

	@Value("${spring.application.name:hello}")
	private String name;

	@GetMapping("/greeting")
	public void sayHello(HttpServletResponse response) {
		// this one does not have a jsp file. it handles the response directly
		try {
			response.getWriter().println("Hello from the orders greeting. Welcome to " + name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// @PostMapping will not permit get method requests
	@PostMapping("/addOrder")
	public ModelAndView addProduct(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName) {

		ModelAndView mv = new ModelAndView();

		mv.setViewName("orders");
		mv.addObject("customer_name", lastName + "," + firstName);

		return mv;

	}

}

package com.mthree.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mthree.services.ProductService;

@Controller
@RequestMapping("/")
/* The @Request-Mapping annotation, when applied at the class level, specifies the kind of requests that this controller handles.
For example, narrow down what this controller will accept by adding to the mapping path:
@RequestMapping("/orders")
*/ 
public class HomeController {

	@Value("${spring.application.name:hello}")
	private String name;

    @Autowired
    private ProductService productService;

    // you will see RequestMapping in many application as an annotation on a methods, so you need to be able to read and understand it.
    //GetMapping is the newer annotation, which supports the parameter "consumes".
    //@RequestMapping("/")
    @GetMapping("/")
    public String hello(){
    	// Whatever is returned here will be mapped to a file with .jsp appended
        return name;
    }


    @GetMapping("/greeting")
    public void sayHello(HttpServletResponse response){
    	// this one does not have a jsp file. it handles the response directly
        try {
            response.getWriter().println("Welcome to " + name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @GetMapping("/viewProducts")
    public ModelAndView viewProducts(){
    	// this one will create a combination model and view object that contains all the model objects. 
    	// products.jsp will read the objects that it wants
        ModelAndView mv = new ModelAndView();

        mv.setViewName("products");

        mv.addObject("product_list",productService.createProductList());
        mv.addObject("lucky_number", Math.round(Math.random() * 100));

        return mv;


    }
    
	// @RequestParam is used to read the user input that come from HTML form
	// Spring will unpack the form and populate the parameters to this method
    @GetMapping("/addCustomer")
    public ModelAndView addProduct(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName){
    	// we can write the firstName and lastName onto the console ( :( ) to see the contents are what we expect
    	System.out.println("The addCustomer get mapping received: " + firstName + " " + lastName);
    	// this will return nothing as the result for the browser to render
       return null;


    }
    
    

    
    
    
    
    
    
    
    


}

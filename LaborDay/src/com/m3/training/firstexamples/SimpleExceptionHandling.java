package com.m3.training.firstexamples;

import java.io.IOException;
import java.util.Random;

public class SimpleExceptionHandling {
	private int someResource = 0;

	public static void main(String[] args) {
		
		SimpleExceptionHandling seh = new SimpleExceptionHandling();
		seh.doWrapUpExceptions();
		seh.doTryWithResource();
		seh.doFinallyOnlyExample();
		seh.doFinallyExample();
		seh.doCheckedExample();
		seh.doUncheckedExample();

	}

	private void doWrapUpExceptions() {
		try {
			wrapUpSomeoneElsesProblem();
		}
		catch (CustomUncheckedException | CustomCheckedException e) {
			System.out.println("Dear customer " + e.getMessage());
			System.out.println("Dear QA team or sys admin " + e.getCause().getMessage());
		}	
	}

	private void wrapUpSomeoneElsesProblem() throws CustomCheckedException {
		try {
			doM5();
		}
		catch (ArithmeticException e) {
			String msg = "The user doesn't really care about the tech details. Application Exception. ";
			throw new CustomCheckedException(msg, e);
		}
	}

	private int doM5() {
		return 6 / (new Random().nextInt(2));
	}

	private void doTryWithResource() //throws Exception 
	{
		try (Resource r = new Resource()) {
			System.out.println("Use my resource: " + r);
			doUncheckedExample();
			System.out.println("Finished with resource: " + r);
		}
		catch (IllegalStateException | IllegalArgumentException e) {
			System.out.println("resolve state problem: " + e.getMessage());
		}
	}

	private void doFinallyOnlyExample() {
		int validParameter = 4;

		
		try {
			this.someResource = 777;
			int parameter = new Random().nextInt(3);
			doM4(parameter);
			System.out.println("Try block finished its work successfully: " + this.someResource);
		}
		finally {
			// do some kind of cleanup
			this.someResource = 0;
			System.out.println("Finally block finished its work successfully: " + this.someResource);

		}
		System.out.println("doFinallyExample finished its work successfully.");

	}
	private void doFinallyExample() {
		int validParameter = 4;
	
		try {
			this.someResource = 777;
			int parameter = new Random().nextInt(3);
			doM4(parameter);
			System.out.println("Try block finished its work successfully: " + this.someResource);
		}
		catch (IllegalArgumentException e) {
			// solve argument problem
			doM4(validParameter);
			System.out.println("Catch block finished its work successfully: " + this.someResource);
		}
		finally {
			// do some kind of cleanup
			this.someResource = 0;
			System.out.println("Finally block finished its work successfully: " + this.someResource);

		}
		System.out.println("doFinallyExample finished its work successfully.");

	}

	private void doUncheckedExample() {
		int parameter = new Random().nextInt(3);
		doM4(parameter);
	}

	private void doM4(int parameter) {
		if (parameter < 2) {
			String msg = "Cannot permit parameter with value less than 2: " + parameter + ".";
			throw new IllegalArgumentException(msg);
		}
	}

	private void doCheckedExample() {
		try {
			doM2();
			System.out.println("Regular M1 business continues here");
		}
		catch (IOException e) {
			String msg = e.getMessage();
			System.out.println("Try to resolve this problem: " + msg);
		}
		System.out.println("Continue with more regular business");
	}

	private void doM2() throws IOException {
		doM3();
	}

	private void doM3() throws IOException {

		if (new Random().nextInt(3) < 2) {
			// never throw unconditionally
			// detects problem
			String msg = "Cannot open file request by user: blablablal";
			throw new IOException(msg);
		}
		// regular business continues
		System.out.println("Regular successful M3 business");
	}

}

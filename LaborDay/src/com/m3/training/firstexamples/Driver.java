package com.m3.training.firstexamples;

import java.util.ArrayList;
import java.util.List;

public class Driver {
	
	public static void main(String[] args) {
		
		List<IBirdFlight> cage = new ArrayList<>();
		
		cage.add(new Loon());
		cage.add(new Loon());
		//cage.add(new Penguin());
		
		for (IBirdFlight bird : cage) {
			System.out.println(bird.getClass().getName() + " " + bird.fly());
		}
	}

}

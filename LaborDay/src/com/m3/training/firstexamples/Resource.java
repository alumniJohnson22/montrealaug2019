package com.m3.training.firstexamples;

import java.util.Random;

public class Resource implements AutoCloseable {

	@Override
	public void close() throws IllegalStateException 
	{
		if (new Random().nextInt(3) < 1) {
			throw new IllegalStateException("Could not close resource");
		}
		System.out.println("Resource is closing");
	}

}

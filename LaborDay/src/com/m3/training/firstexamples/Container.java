package com.m3.training.firstexamples;

import java.util.ArrayList;
import java.util.List;

public class Container<T> {
	private T t;

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}
	
	public int doWork() {
		return t.hashCode();
	}
	
	public List<T> wrapObject(T t) {
		ArrayList<T> list = new ArrayList<>();
		list.add(t);
		return list;
	}
	
	public static void main() {
		Container<Object> co = new Container<>();
		co.setT(new Object());
		co.setT(new Card());
		
		Container<Card> cc = new Container<>();
		cc.setT(new Card());
		//cc.setT(new Object());		
	}
}

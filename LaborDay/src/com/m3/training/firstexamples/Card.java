package com.m3.training.firstexamples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Card implements Comparable<Card> {
	// suit 0-Spade 1-Heart 2-Clubs 3-Diamonds
	// rank A-1 2-10 Jack-11 Queen-12 King-13
	// suit*13 + (rank-1)
	
	// alternative
	// just take the suit, give 4 buckets with 13 collisions each
	// just take the rank, give 13 buckets with 4 collisions
	
	private int suit;
	private int rank;
	
	public int getSuit() {
		return suit;
	}

	public void setSuit(int suit) {
		this.suit = suit;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	@Override
	public int hashCode() {
		return suit*13 + (rank-1);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (rank != other.rank)
			return false;
		if (suit != other.suit)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Card [suit=" + suit + ", rank=" + rank + "]";
	}

	@Override
	public int compareTo(Card obj) {
		// suit 0-Spade 1-Heart 2-Clubs 3-Diamonds
		// rank A-1 2-10 Jack-11 Queen-12 King-13
		// suit*13 + (rank-1)
		if (this.equals(obj)) {
			return 0;
		}
	
		int hashCodeForThis = this.hashCode();
		int hashCodeForObj = obj.hashCode();
		return (hashCodeForThis - hashCodeForObj);
		
	}
	
	public static void main(String[] args) {
		Card card1 = new Card();
		// Ace of Spade
		card1.suit = 0;
		card1.rank = 1;
		Card card2 = new Card();
		card2.suit = 0;
		card2.rank = 1;
		System.out.println("Card1 " + card1);
		System.out.println("Card2 " + card2);
		System.out.println("Card1 equals card2? " + card1.equals(card2));
		System.out.println("Card1 greater than card2? " + card1.compareTo(card2));
		Card card3 = new Card();
		card3.suit = 3;
		card3.rank = 13;
		System.out.println("Card1 hashCode? " + card1.hashCode());
		System.out.println("Card3 hashCode? " + card3.hashCode());
		System.out.println("Card1 greater than card3? " + card1.compareTo(card3));
		Card card4 = new Card();
		card4.suit = 2;
		card4.rank = 13;
		Card card5 = new Card();
		card5.suit = 1;
		card5.rank = 7;
		List<Card> hand = new ArrayList<Card>();
		hand.add(card5);
		hand.add(card3);
		hand.add(card2);
		hand.add(card4);
		hand.add(card1);
		System.out.println("First random " + hand);
		Collections.sort(hand);
		System.out.println("Sorted " + hand);
		Collections.shuffle(hand);
		System.out.println("Second random " + hand);
		Collections.sort(hand, new UnoCardComparator());
		System.out.println("Uno " + hand);
	}






}

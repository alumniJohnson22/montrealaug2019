package com.m3.training.firstexamples;

public class Circle extends Shape {

	private double radius;

	public Circle() {
		this(1);
	}

	public Circle(double radius) {
		super(0x0, 0xfffffff);
		this.setRadius(radius);
	}
	
	private void setRadius(double radius) {
		if (radius < 0) {
			radius = radius * (-1);
		}
		this.radius = radius;
	}

	public static void main(String[] args) {
		System.out.println(args[0] + " " + args[1] + " " + args[2]);
		Circle circle = new Circle();
		circle.setLineColor(0xfedcba);
		//avoid sysout - you are a pro!
		System.out.println(circle);
	}

	@Override
	public String toString() {
		return super.toString() + " Circle [radius=" + radius + "]";
	}

	@Override
	public void draw() {
		System.out.println("Drawing a circle of radius " + this.getRadius());
	}

	private double getRadius() {
		return this.radius;
	}
	
	
	
}

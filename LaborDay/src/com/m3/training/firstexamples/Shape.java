package com.m3.training.firstexamples;

public abstract class Shape {
	private static final int DEFAULT_FILL = 0xffffff;
	private static final int DEFAULT_LINE = 0x000000;
	
	private int fillColor = Shape.DEFAULT_FILL;
	private int lineColor = Shape.DEFAULT_LINE;
	
	public Shape(int fillColor, int lineColor) {
		super();
		this.setFillColor(fillColor);
		this.setLineColor(lineColor);
	}
	
	public int getFillColor() {
		return fillColor;
	}
	public void setFillColor(int fillColor) {
		this.fillColor = fillColor;
	}
	public int getLineColor() {
		return lineColor;
	}
	public void setLineColor(int lineColor) {
		// logic to test the viability of the color
		this.lineColor = lineColor;
	}
	
	@Override
	public String toString() {
		String msg = super.toString() + " fill color is " + this.getFillColor() + " line color " + this.getLineColor();
		return msg;
	}
	
	public abstract void draw();
	
}


package com.m3.training.firstexamples;

public class WildCardDemo {
	
	public static void main() {
		Container<Object> containsObject = new Container<>();
		// consume an object
		containsObject.setT(new Object());
		// supply an object
		Object obj = containsObject.getT();
		
		Container<Card> containsCard = new Container<>();
		containsCard.setT(new Card());
		Card card = containsCard.getT();
	
		// inversion of control 
		Container<Account> containsAccount = new Container<>();
		containsAccount.setT(new Account());
		
		Container<SavingsAccount> containsSavings = new Container<>();
		containsSavings.setT(new SavingsAccount());
		
		Container<CheckingAccount> containsChecking = new Container<>();
		containsChecking.setT(new CheckingAccount());		
		
		// PECS: producer extends, consumer super
		// cannot write using the extends wild card reference, but we can read 
		Container<? extends Account> containerSupply;
		containerSupply = containsSavings;
		containerSupply = containsChecking;
		containerSupply = containsAccount;
		containerSupply.setT(new Account());		
		containerSupply.setT(new CheckingAccount());
		containerSupply.setT(new SavingsAccount());
		Account account = containerSupply.getT();
	
		// cannot read using the super wild card reference, but we can write
		Container<? super CheckingAccount> containerConsume;
		containerConsume = containsChecking;
		containerConsume = containsAccount;	
		containerConsume = containsObject;
		containerConsume.setT(new CheckingAccount());
		obj = containerConsume.getT();
		CheckingAccount checking = containerConsume.getT(); 
	}

}

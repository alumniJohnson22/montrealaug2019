package com.m3.training.firstexamples;

public class StringPool {
	public static void main(String[] args) {
		String s1 = "Hello World";
		String s2 = s1;
		String s3 = "Hello World";
		String s4 = new String(s1);
		String s5 = new String("Hello World");
		String s6 = new String("Goodbye");	
		
		System.out.println(System.identityHashCode(s1) + " is address of s1");
		System.out.println(System.identityHashCode(s2) + " is address of s2");		
		System.out.println(System.identityHashCode(s3) + " is address of s3");		
		System.out.println(System.identityHashCode(s4) + " is address of s4");		
		System.out.println(System.identityHashCode(s5) + " is address of s5");
		System.out.println(System.identityHashCode(s6) + " is address of s6");
		
//		boolean check = (s1 == s2);
//		System.out.println("Is s1 " + s1 + " == to s2 " + s2 + " " + check);
//		check = s1.equals(s2);
//		System.out.println("Is s1 " + s1 + " == to s2 " + s2 + " " + check);		
//		check = (s1 == s3);
//		System.out.println("Is s1 " + s1 + " == to s3 " + s3 + " " + check);
//		check = s1.equals(s3);
//		System.out.println("Is s1 " + s1 + " == to s3 " + s3 + " " + check);	
//		check = (s1 == s4);
//		System.out.println("Is s1 " + s1 + " == to s4 " + s4 + " " + check);
//		check = s1.equals(s4);
//		System.out.println("Is s1 " + s1 + " == to s4 " + s4 + " " + check);
//		s1 = "Graham's change";
//		System.out.println(s1 + " " + s2);
//		
//		s1 = "Goodbye";
//		s2 = "helloWo";
//		int result = s1.compareTo(s2);
//		System.out.println(s1 + " " + s2 + " left sorts before parameter? " + result);
//		
		String fakeNetworkDevice = args[0];
		String ticker = fakeNetworkDevice;
		System.out.println(System.identityHashCode(args[0]) + " is address of args[0]");		
		System.out.println(System.identityHashCode(fakeNetworkDevice) + " is address of fakeNetworkDevice");
		System.out.println(System.identityHashCode(ticker) + " is address of ticker");

		ticker.intern();
		String nextTick = fakeNetworkDevice;
		System.out.println(System.identityHashCode(ticker) + " is address of ticker");
		nextTick.intern();	
		System.out.println(System.identityHashCode(nextTick) + " is address of nextTick");

		nextTick = args[2];
		System.out.println(System.identityHashCode(nextTick) + " is address of nextTick");
	
//		// not a proof! because hashcode is not an address for String
//		System.out.println("Address of fakeNextWorkDevice " + fakeNetworkDevice.hashCode());
//		System.out.println("Address of ticker " + ticker.hashCode());
//		System.out.println("Address of nextTick " + nextTick.hashCode());
//		System.out.println("Address of args[0] " + args[0].hashCode());	
//		nextTick = args[1];
//		System.out.println("Address of nextTick " + nextTick.hashCode());
//		nextTick = args[2];
//		System.out.println("Address of nextTick " + nextTick.hashCode());	
//
//		String s10 = "I have a common prefixGH";
//		String s11 = "I have a common prefixCa";
//		System.out.println("s10 " + s10.hashCode());
//		System.out.println("s11 " + s11.hashCode());
		}
}

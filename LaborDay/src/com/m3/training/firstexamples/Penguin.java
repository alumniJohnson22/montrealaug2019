package com.m3.training.firstexamples;

public class Penguin extends Bird {
	
	// violation of Liskov
	public String fly() {
		return "No.";
	}

}

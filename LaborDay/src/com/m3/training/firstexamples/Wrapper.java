package com.m3.training.firstexamples;

import java.util.ArrayList;
import java.util.List;

public class Wrapper {
	
	public <T> List<T> wrapObject(T t) {
		ArrayList<T> list = new ArrayList<>();
		list.add(t);
		return list;
	}
	
	

}

package com.m3.training.firstexamples;

public class Loon extends Bird implements IBirdFlight {

	@Override
	public String fly() {
		return "skim over the waters";
	}

}

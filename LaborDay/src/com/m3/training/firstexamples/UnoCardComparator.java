package com.m3.training.firstexamples;

import java.util.Comparator;

public class UnoCardComparator implements Comparator<Card> {

	@Override
	public int compare(Card card1, Card card2) {
		if (card1.getSuit() == card2.getSuit() ||
				card1.getRank() == card2.getRank())  {
			return 0;
		}
		return card2.getRank() - card1.getRank();
	}

}

package com.mthree.apps;

public class Product {

    private String productCode;

    private String name;

    private String exchange;

    private String category;

    private String subCategory;

    private String pClass;

    private String sze;

    public Product() {}
    
    public Product(String productCode, String name, String exchange, String category, String subCategory, String pClass, String sze) {
        this.productCode = productCode;
        this.name = name;
        this.exchange = exchange;
        this.category = category;
        this.subCategory = subCategory;
        this.pClass = pClass;
        this.sze = sze;
    }


    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getpClass() {
        return pClass;
    }

    public void setpClass(String pClass) {
        this.pClass = pClass;
    }

    public String getSze() {
        return sze;
    }

    public void setSze(String sze) {
        this.sze = sze;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productCode='" + productCode + '\'' +
                ", name='" + name + '\'' +
                ", exchange='" + exchange + '\'' +
                ", category='" + category + '\'' +
                ", subCategory='" + subCategory + '\'' +
                ", pClass='" + pClass + '\'' +
                ", sze='" + sze + '\'' +
                '}';
    }
}

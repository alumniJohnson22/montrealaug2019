package com.mthree.apps;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class SpringConfig {
	
	
	@Bean // Default scope is Singleton 
	public Product product1() {
		Product p1 = new Product("ED","Eurodollar Futures","CME","InterestRate","Stirs","Future","NULL");
		return p1;
	}

	
	@Bean
	@Scope(value="singleton") // default - Spring creates one and only one object per application 
	public Fix fix1() {
		
		
		Fix f1 = new Fix(1, "35", "MsgType", 'U' , "A \"U\" as the first character (i.e. U1, U2, etc) indicates that the message format is privately defined between the sender and receiver.");
		
		return f1;
		
	}
	
	@Bean
	@Scope(value="prototype") // Spring creates object per request
	public Fix fixPrototype() {
		
		Fix f1 = new Fix(1, "35", "MsgType", 'U' , "A \"U\" as the first character (i.e. U1, U2, etc) indicates that the message format is privately defined between the sender and receiver.");
		
		return f1;
		
	}
	
	
}

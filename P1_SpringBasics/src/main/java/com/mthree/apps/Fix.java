package com.mthree.apps;

public class Fix {
	
	private int id;
	
	private String tag;
	
	private String name;
	
	private char value;
	
	private String descr;

	public Fix() {}
	
	public Fix(int id, String tag, String name, char value, String descr) {
		super();
		this.id = id;
		this.tag = tag;
		this.name = name;
		this.value = value;
		this.descr = descr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getValue() {
		return value;
	}

	public void setValue(char value) {
		this.value = value;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	@Override
	public String toString() {
		return "Fix [id=" + id + ", tag=" + tag + ", name=" + name + ", value=" + value + ", descr=" + descr + "]";
	}
	
	

}

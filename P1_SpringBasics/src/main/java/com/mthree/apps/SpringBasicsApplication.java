package com.mthree.apps;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication

public class SpringBasicsApplication implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication.run(SpringBasicsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// DEBUG ME

		// Step 1 - create an ApplicationContext
		// Spring reads spring.xml file and creates instances for all the bean
		// definitions (<bean> elements)
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

		// Spring reads SpringConfig class and creates instances for all the bean
		// definitions (methods with @Bean)
		try (AnnotationConfigApplicationContext annotationContext = new AnnotationConfigApplicationContext(
				SpringConfig.class)) {

			// Step 2 - Request Product beans and display

			Product p = (Product) context.getBean("p1");
			System.out.println(p);

			Product p2 = (Product) context.getBean("p2");
			System.out.println(p2);

			// Gets a Fix object with singleton scope
			Fix fix1 = (Fix) annotationContext.getBean("fix1");

			Fix fix2 = (Fix) annotationContext.getBean("fix1");

			System.out.println(fix1.getName());

			System.out.println(fix2.getName());

			// Let's modify the name of the fix

			fix1.setName("New name");

			System.out.println(fix1.getName()); // Displays New name

			System.out.println(fix2.getName()); // Displays New name because fix1 and fix2 references refer same objects

			Fix fixPrototype1 = (Fix) annotationContext.getBean("fixPrototype");

			Fix fixPrototype2 = (Fix) annotationContext.getBean("fixPrototype");

			fixPrototype1.setName("New name");

			System.out.println(fixPrototype1.getName()); // Displays New name

			System.out.println(fixPrototype2.getName()); // Displays old name (Eg: MsgType)

		}
	

	}

}

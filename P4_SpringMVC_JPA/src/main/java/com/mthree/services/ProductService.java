package com.mthree.services;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.mthree.models.Product;
import com.mthree.repositories.ProductRepository;

import java.util.List;

@Service
public class ProductService {


    @Autowired
    private ProductRepository productRepository;

    public List<Product> createProductList(){
        return productRepository.findAll();
    }

	public Product getProduct(String id) {
		return productRepository.getOne(id);
	}



}

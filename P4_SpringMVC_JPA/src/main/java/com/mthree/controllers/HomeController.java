package com.mthree.controllers;


import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mthree.services.ProductService;

@Controller
public class HomeController {


    @Autowired
    private ProductService productService;


    @RequestMapping("/")
    public String hello(){
        return "hello";
    }


    @RequestMapping("/greeting")
    public void sayHello(HttpServletResponse response){

        try {
            response.getWriter().println("Welcome");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @RequestMapping("/viewProducts")
    public ModelAndView viewProducts(){

        ModelAndView mv = new ModelAndView();

        mv.setViewName("products");

        mv.addObject("product_list",productService.createProductList());

        return mv;
    }
    
    @RequestMapping("/viewProduct/{id}")
    public ModelAndView viewProduct(@PathVariable("id") String id){

        ModelAndView mv = new ModelAndView();

        mv.setViewName("oneProduct");

        mv.addObject("product", productService.getProduct(id));

        return mv;
    }
    
    // no parameter means default
    @RequestMapping()
    // This annotation says to give the return value as the body of the response
    @ResponseBody
    public String catchAllMethod(){
    	return "This is our default.";
    }

    @RequestMapping("*")
    // This annotation says to give the return value as the body of the response
    @ResponseBody
    public String catchAllMethod404(){
    	return "We don't know what you are asking.";
    }
}

package com.mthree.apps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

// In the previous project example we used the annotation parameter to give the scanner instructions.
// Here we split them.
@SpringBootApplication
// look here for classes the Soring needs
@ComponentScan({"com.mthree.controllers",
     			"com.mthree.configs",
     			"com.mthree.services",
       			"com.mthree.repositories",
				"com.mthree..models"})
// look here for the entities JPA needs and the database will hold
@EntityScan("com.mthree.models")
// look here to find daos to create
@EnableJpaRepositories(basePackages= {"com.mthree.repositories"})
public class SpringDataJPAApplication {

	public static void main(String[] args) {
		
	            SpringApplication.run(SpringDataJPAApplication.class, args);

	}

}


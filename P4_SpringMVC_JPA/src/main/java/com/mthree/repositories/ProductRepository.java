package com.mthree.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mthree.models.Product;

// will create a DAO for Product for you
@Repository
public interface ProductRepository extends JpaRepository<Product,String> {

	

}

<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.mthree.models.Product"%>
<%@page import="java.util.List"%>

<html lang="en">
<head>
<meta charset="UTF-8" />
<title>Title</title>
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<h1>Product List</h1>



	Here is the product you requested:
	<table border="1">

		<tr>
			<th>Product Code</th>
			<th>Name</th>
			<th>Exchange</th>
			<th>Category</th>
			<th>Sub Category</th>
			<th>Class</th>
			<th>SZE</th>
		</tr>

		<tr>
			<td><c:out value="${product.productCode}" /></td>
			<td><c:out value="${product.name}" /></td>
			<td><c:out value="${product.exchange}" /></td>
			<td><c:out value="${product.category}" /></td>
			<td><c:out value="${product.subCategory}" /></td>
			<td><c:out value="${product.pClass}" /></td>
			<td><c:out value="${product.sze}" /></td>
		</tr>
	</table>




</body>
</html>
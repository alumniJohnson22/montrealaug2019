package com.m3.training;

public interface Identifiable {
	public String getID();
}

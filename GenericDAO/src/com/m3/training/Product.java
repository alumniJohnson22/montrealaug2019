package com.m3.training;

import java.math.BigDecimal;
import java.util.List;

public class Product extends BaseORM implements Identifiable {

	private BigDecimal price;
	private BigDecimal quantity;

	public Product(String name, BigDecimal price, BigDecimal quantity) {
		this.setID(name);
		this.setPrice(price);
		this.setQuantity(quantity);
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Product [price=" + price + ", quantity=" + quantity + "]";
	}

	public static Product create(List<Object> fields) {
		String name = (String) fields.get(0);
		BigDecimal price = (BigDecimal) fields.get(1);
		BigDecimal quantity = (BigDecimal) fields.get(2);
		return new Product(name, price, quantity);
	}
	
	

}

package com.m3.training;

public class Driver {
	public static void main(String[] args) throws Exception {
		DAO<Product> dao = new DAO<>("MON11GROCERIES", "name");
		Product product =  dao.read("carrot");
		System.out.println("Here is the product: " + product);
		dao.close();
		
		DAO<Fruit> dao2 = new DAO<>("sammy_fruits", "fruit_id");
		Fruit myFruit =  dao2.read("1");
		System.out.println("Here is the product: " + myFruit);
		dao2.close();
	}

}

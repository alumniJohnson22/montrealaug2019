package com.m3.training;

import java.sql.SQLException;

public interface CRUD<T> {
	
	public T read(String id) throws SQLException;
}

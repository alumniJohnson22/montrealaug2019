package com.m3.training.calculator;

public class Calculator {
	public static final int INPUT_BUFFER_LIMIT = 1024;
	
	public String calc(String input) {
		if (input == null) {
			String msg = "Null input is not permitted.";
			throw new IllegalArgumentException(msg);
		}
		input = input.trim();
		if (input.length() == 0) {
			String msg = "Empty input is not permitted.";
			throw new IllegalArgumentException(msg);
		}
		if (input.length() > INPUT_BUFFER_LIMIT) {
			String msg = "Input larger than "+ INPUT_BUFFER_LIMIT +" is not permitted.";
			throw new IllegalArgumentException(msg);
		}
		String result = null;
		String[] pieces = input.split("\\s+");
		if (pieces.length == 1) {
			double value = Double.parseDouble(input);
			result = input;
		}
		return result;
	}

}

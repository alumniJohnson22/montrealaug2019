package com.m3.training.calculator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class CalculatorTest {
	private Calculator classUnderTest;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		classUnderTest = new Calculator();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	// this is not a real test, even though it looks like one
	@Test
	void test() {
		assertNotNull(classUnderTest);
	}

	@Test
	void test_Calculator_simpleNumber() {
		String msg = "Could not calculate one number.";
		String input = "5";
		String actual = classUnderTest.calc(input);
		String expected = "5";
		assertEquals(expected, actual, msg);
	}
	
	@Test
	void test_Calculator_nullInput() {
		String msg = "Did not reject null input.";
		String input = null;
		Executable closure = () -> {classUnderTest.calc(input);};
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	@Test
	void test_Calculator_emptyInput() {
		String msg = "Did not reject empty input.";
		String input = "";
		Executable closure = () -> {classUnderTest.calc(input);};
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	@Test
	void test_Calculator_whiteSpaceInput() {
		String msg = "Did not reject whitespace input.";
		String input = "";
		Executable closure = () -> {classUnderTest.calc(input);};
		assertThrows(IllegalArgumentException.class, closure, msg);
	}

	@Test
	void test_Calculator_overlyLargeInput() {
		String msg = "Did not reject overly large input.";
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index <= Calculator.INPUT_BUFFER_LIMIT; index++) {
			sb.append("9");
		}
		String input = sb.toString();
		Executable closure = () -> {classUnderTest.calc(input);};
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
}







